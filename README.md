# HW #3 - Machine Learning | CSci 435 F17 | College of William & Mary

## Introduction

Machine learning is one of the fastest-growing subfields of computer science - and for good reason. With the recent introduction of superior hardware, neural networks have become far more effective and have become relevant to a wide range of problems.

This assignment is to perform classification images of handwritten digits. You will gain hands-on experience with TensorFlow and Keras. While it has become much easier to install TensorFlow and Keras in the past year, they still can be a headache. We don't want this assignment to be all about installing packages, so we'll use Kaggle's **Kernels**, a free platform for data science. 

## Assignment

### Kaggle

This is a classic example of an image classification problem, so it's a great opportunity to use a Convolutional Neural Network (CNN). 

First, register for an account at <a href='https://kaggle.com'>Kaggle</a>. 

Once you have an account set up, head over to <a href="https://www.kaggle.com/kernels">Kaggle Kernels</a> and make a new Notebook. Next, click **Add Data Source**, click the **Competitions** tab and select **Digit Recognizer**. Your notebook may take some time to start up - don't worry if it seems slow at first.

### Load Data

After the notebook gets going, we'll take care of inputs for the rest of the assignment. Paste the following into a cell:

```python
import matplotlib.pyplot as plt
%matplotlib inline

from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix

from keras.utils.np_utils import to_categorical # convert to one-hot-encoding
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPool2D, BatchNormalization
from keras.optimizers import Adam
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import LearningRateScheduler
```

Next, we'll set up files, load the data, and separate the data into a training set and a validation set. We need a validation set to evaluate the performance of our model. Paste and run the following:

```python
train_file = "../input/train.csv"
test_file = "../input/test.csv"
output_file = "submission.csv"

raw_data = np.loadtxt(train_file, skiprows=1, dtype='int', delimiter=',')
x_train, x_val, y_train, y_val = train_test_split(raw_data[:,1:], raw_data[:,0], test_size=0.1)
```

Now that we have the data, let's take a look at it. Choose an index from x_train and run the code that's below.

```python
plt.imshow(x_train[ your_index_here ].reshape(28,28), cmap='gray')
```

Next, we'll specify that this data has only 1 channel because it's grayscale, not color imagery. We'll also divide the intensities by 255 so that the values are between 0 and 1 which will make the CNN train faster. Paste and run the code below:

```python
x_train = x_train.reshape(-1, 28, 28, 1)
x_val = x_val.reshape(-1, 28, 28, 1)

x_train = x_train.astype("float32")/255.
x_val = x_val.astype("float32")/255.
```

Lastly, we'll convert each of the images' classes to one-hot encoding; that is, a 10-cell array with a single 1 and nine 0s. The 1 will be at the index of the class. For instance, 3 would look like [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]. Paste the code below to get one-hot encoding for our y values.

```python
y_train = to_categorical(y_train)
y_val = to_categorical(y_val
```

Choose an index from y_train and run the code below to see what `to_categorical` does.

```python
print(y_train[ your_index_here ])
```

### Train the Model

We're going to use Keras's Sequential API to build a CNN. With the Sequential API, you add one layer at a time from the input to the output. 

Paste and run the following to define your model. 

```python
model = Sequential()

model.add(Conv2D(filters = 16, kernel_size = (3, 3), activation='relu',
                 input_shape = (28, 28, 1)))
model.add(BatchNormalization())
model.add(Conv2D(filters = 16, kernel_size = (3, 3), activation='relu'))
model.add(BatchNormalization())
model.add(MaxPool2D(strides=(2,2)))
model.add(Dropout(0.25))

model.add(Conv2D(filters = 32, kernel_size = (3, 3), activation='relu'))
model.add(BatchNormalization())
model.add(Conv2D(filters = 32, kernel_size = (3, 3), activation='relu'))
model.add(BatchNormalization())
model.add(MaxPool2D(strides=(2,2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(512, activation='relu'))
model.add(Dropout(0.25))
model.add(Dense(1024, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(10, activation='softmax'))
```

Next, we'll augment the data by adding random transformations to the training set. This way, the classifier will be better at classifying rotated or otherwise perturbed images. Use the following code to augment your data, and consider tuning these parameters to get different results

```python
datagen = ImageDataGenerator(zoom_range = 0.1,
                            height_shift_range = 0.1,
                            width_shift_range = 0.1,
                            rotation_range = 10)
```

Next, compile the model and define a loss function and optimizer. These are what let the model determine whether or not it's getting better. Use the code below:

```python
model.compile(loss='categorical_crossentropy', optimizer = Adam(lr=1e-4), metrics=["accuracy"])
```

Finally, we'll run the model. **Set the number of epochs to 1 for your first time running the model.** **Set it to 20 for your second submission**. Set it to no more than 20 when using Kernels - if you can get Keras and TensorFlow to run locally, set it as high as you want.  **This section may take up 20 minutes to run.** 

```python
annealer = LearningRateScheduler(lambda x: 1e-3 * 0.9 ** x)
hist = model.fit_generator(datagen.flow(x_train, y_train, batch_size=16),
                           steps_per_epoch=500,
                           epochs= your_epochs_here,
                           verbose=2,
                           validation_data=(x_val[:400,:], y_val[:400,:]),
                           callbacks=[annealer])
```

### Submit Test Set

Now that the model has been trained, we can fit to the test data. Use the following code to obtain the test set:

```python
mnist_testset = np.loadtxt(test_file, skiprows=1, dtype='int', delimiter=',')
x_test = mnist_testset.astype("float32")
x_test = x_test.reshape(-1, 28, 28, 1)/255.
```

Finally, fit the data:

```
y_hat = model.predict(x_test, batch_size=64)
y_pred = np.argmax(y_hat,axis=1)
```

Use this to get your files for submission:

```python
with open(output_file, 'w') as f :
    f.write('ImageId,Label\n')
    for i in range(len(y_pred)) :
        f.write("".join([str(i+1),',',str(y_pred[i]),'\n']))
```

Once all the cells in the notebook have finished running, click **Publish** at the top-right of the screen. It will take some time (15-30 minutes typically) before the output is visible in a tab. Once you can get to the output, download the CSV output, and submit it to the MNIST competition for your 1 epoch model. Do the same for the 20 epoch model afterward. Once you have made your submissions, email a link to your profile to your TA.

## Submission 

1. Make a Kaggle competition submission using 1 epoch
2. Make a Kaggle competition submission using 20 epochs
3. Share a link to your profile with your TA

## Citations

Assignment adapted from:  https://www.kaggle.com/toregil/welcome-to-deep-learning-cnn-99